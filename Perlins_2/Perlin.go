package main

import (
	"fmt"
	"math"
	// "math"
	// "fmt"
	"math/rand"
)

var per []int
var per2 []int

// Coordinate a
type Coordinate struct {
	w, h int
}

func main() {

	Coordinates := Coordinate{10, 10}

	// symbols := []string{"a", "b", "c", "d", "e", "f"}

	fmt.Printf("%v", gridDefinition(Coordinates))

	var x int = Coordinates.w
	var y int = Coordinates.h
	var finalNoise = make([]float32, x*y)
	var finalSymbol = make([]int, x*y)

	go func(noise [][]float32, Coordinates Coordinate, x, y int) {

		for i := 0; i < x; i++ {
			for j := 0; j < y; j++ {
				finalNoise[j*i] = dotProduct(noise, Coordinates, i, j)

				// fmt.Printf("%v \n" , int(finalNoise[i*x+j] / 0.2))
				finalSymbol[j*i] = (j)
			}
		}
	}(gridDefinition(Coordinates), Coordinates, x, y)

	fmt.Printf("%v \n", finalNoise)
	fmt.Printf("%v \n", len(finalNoise))

	fmt.Printf("%v \n", finalSymbol)
	fmt.Printf("%v \n", len(finalSymbol))

	var input string
	fmt.Scanln(&input)
	fmt.Println("DONE")
}

func gridDefinition(Coordinates Coordinate) [][]float32 {
	var x int = Coordinates.w
	var y int = Coordinates.h

	var noise = make([][]float32, x)

	for i := 0; i < x; i++ {
		var noiseY = make([]float32, y)
		noise[i] = noiseY

		for j := 0; j < y; j++ {
			noise[i][j] = rand.Float32()
		}
	}

	return noise
}

func dotProduct(noise [][]float32, Coordinates Coordinate, i, j int) float32 {
	var x int = Coordinates.w
	var y int = Coordinates.h

	// set cons
	var samplePeriod float64 = math.Pow(2, 1)
	var freq float32 = float32(1) / float32(samplePeriod)

	//calculate the horizontal sampling indices
	smple_i0 := (i / int(samplePeriod)) * int(samplePeriod)
	smple_i1 := (smple_i0 + int(samplePeriod)) % x
	h_blend := (float32(i) - float32(smple_i0)) * freq

	// calculate the vertical sampling indices
	smple_j0 := (j / int(samplePeriod)) * int(samplePeriod)
	smple_j1 := (smple_j0 + int(samplePeriod)) % y
	v_blend := (float32(j) - float32(smple_j0)) * freq

	//blend the top two corners
	top := interpolate(noise[smple_i0][smple_j0], noise[smple_i1][smple_j0], h_blend)

	//blend the bottom two corners
	bot := interpolate(noise[smple_i0][smple_j1], noise[smple_i1][smple_j1], h_blend)

	return interpolate(top, bot, v_blend)
}

func interpolate(x0, x1, alpha float32) float32 {
	return x0*(1-alpha) + alpha*x1
}
