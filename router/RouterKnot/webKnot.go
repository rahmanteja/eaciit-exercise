package main

import (
	"fmt"
	"hello/global"
	"os"

	. "github.com/eaciit/knot/knot.v1"
)

func main() {
	ks := new(Server)

	ks.Address = "localhost:" + global.WebPort

	DefaultOutputType = OutputJson

	ks.Route("/knotbasic", Index)

	ks.Listen()
}

// Index a
func Index(w *WebContext) interface{} {
	fmt.Println(os.Getwd())

	return "{a : 'a'}"
}
