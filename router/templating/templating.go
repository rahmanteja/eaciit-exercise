package templating

import (
	"hello/global"
	"html/template"
	"net/http"
)

// ToView a
func ToView(w http.ResponseWriter, page string, data interface{}) {
	// tmpl, err := template.ParseFiles(global.WebHTMLDir + "Template/layout.html")
	tmpl, err := template.ParseFiles(global.WebHTMLDir + page)
	if err != nil {
		panic(err)
	}

	tmpl.Execute(w, data)
}
