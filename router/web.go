package main

import (
	"fmt"
	"hello/global"
	"hello/http/Controller/callGit"
	"hello/http/Controller/dBox"
	"hello/http/Controller/dateTime"
	"hello/http/Controller/filePart"
	"hello/http/Controller/knotbasic"
	"hello/router/templating"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	Init()
}

// Init inisialisai untuk router
func Init() {

	router := mux.NewRouter()
	ServeStatic(router, "../resource/")

	// Router START
	fmt.Println(http.Dir("./resource/public/view/"))
	router.PathPrefix("/Person/").Handler(http.StripPrefix("/Person", http.FileServer(http.Dir("../resource/public/view/")))).Methods("GET")

	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		templating.ToView(w, "Template/Welcome.html", nil)
	}).Methods(http.MethodGet)

	// KNOT BASIC START
	router.HandleFunc("/knotbasic", knotbasic.Index).Methods(http.MethodGet)
	router.HandleFunc("/knotbasic", knotbasic.Index).Methods(http.MethodPost)
	// KNOT BASIC END

	// DATETIME START
	router.HandleFunc("/datetime", dateTime.Index).Methods(http.MethodGet)
	router.HandleFunc("/datetime", dateTime.Index).Methods(http.MethodPost)
	// DATETIME END

	// DBox START
	router.HandleFunc("/dbox", DBoxModel.Index).Methods(http.MethodGet)
	// router.PathPrefix("/dbox/create").Handler(http.FileServer(http.Dir(global.WebHTMLDir + "Person/person.html")))
	router.HandleFunc("/dbox/create", DBoxModel.Create).Methods(http.MethodGet)
	router.HandleFunc("/dbox/store", DBoxModel.Store).Methods(http.MethodPost)
	router.HandleFunc("/dbox/edit/{id}", DBoxModel.Edit).Methods(http.MethodGet)
	router.HandleFunc("/dbox/update/{id}", DBoxModel.Update).Methods(http.MethodPost)
	router.HandleFunc("/dbox/destroy", DBoxModel.Destroy).Methods(http.MethodPost)
	router.HandleFunc("/dbox/search", DBoxModel.Search).Methods(http.MethodGet)
	// DBox END

	// File Upload START
	router.HandleFunc("/upload", filePart.Index).Methods(http.MethodGet)
	router.HandleFunc("/upload/create", filePart.Create).Methods(http.MethodGet)
	router.HandleFunc("/upload/store", filePart.Store).Methods(http.MethodPost)
	router.HandleFunc("/upload/store", filePart.Store).Methods(http.MethodGet)
	router.HandleFunc("/upload/edit/{id}/", filePart.Edit).Methods(http.MethodGet)
	router.HandleFunc("/upload/update/{id}/", filePart.Update).Methods(http.MethodPost)
	router.HandleFunc("/upload/destroy", filePart.Destroy).Methods(http.MethodPost)
	router.HandleFunc("/upload/search", filePart.Search).Methods(http.MethodGet)
	router.HandleFunc("/upload/file/{fname}", filePart.Search).Methods(http.MethodGet) // DOWNLOAD
	// File Upload END

	// Call Git START
	router.HandleFunc("/call/{id}", CallGit.Index).Methods(http.MethodGet)
	// Call Git END

	// Router END
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":"+global.WebPort, router))
}

// ServeStatic a
func ServeStatic(router *mux.Router, staticDirectory string) {
	staticPaths := map[string]string{
		"styles":    staticDirectory + "public/css/",
		"js":        staticDirectory + "public/js/",
		"images":    staticDirectory + "public/img/",
		"libstyles": staticDirectory + "public/lib/css/",
		"libjs":     staticDirectory + "public/lib/js/",
		"views":     staticDirectory + "view/",
	}
	for pathName, pathValue := range staticPaths {
		pathPrefix := "/" + pathName + "/"
		router.PathPrefix(pathPrefix).Handler(http.StripPrefix(pathPrefix,
			http.FileServer(http.Dir(pathValue))))
	}
}
