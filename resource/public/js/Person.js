var row = 1;
var pers;
var arr_pers;

$(document).ready(function() {
    // alert("asa");
    index();

    $("body").on("click" , ".edit" , function(){
        var idNow = $(this).data("row");

        var personx = $.grep(arr_pers , function(e) {
            // console.log(e.id);
            return e.id == idNow;
        });
        
        pers = new person();

        clearForm();

        pers.setID(idNow);
        pers.setName(personx[0].name);
        pers.setBirthDate(personx[0].birthdate);

        $.each(personx[0].parent , function(k , v) {
            if(row == 2) {
                $("#par-name-" + (row - 1)).val(v);
                // row--;
            }
            else {
                $("#form-parent").append(parent(row - 1));

                $("#par-name-" + (row - 1)).val(v);
            }
            pers.addParentIndex(row - 1 , v);

            row++;
        });

        $("#id").val(pers.getID());
        $("#name").val(pers.getName());
        $("#birthdate").val(pers.getBirthDate());

        
        
    });

    $("#btn-new").click(function(){
        pers = new person();

        clearForm();
    })

    $(".add-parent").click(function(){
        $("#form-parent").append(parent(row));

        row++;
    });

    $("body").on("click" , ".del-parent" , function(){
        $("#parent-" + $(this).data("parentrow")).remove();

        pers.deleteParent($(this).data("parentrow"));
    });

    $("#save").click(function(){
        var urlx = "";
        if (pers.id == 0 || pers.id == undefined) {
            // Save
            urlx = "http://localhost:8080/dbox/store";
        }
        else {
            urlx = "http://localhost:8080/dbox/update/" + pers.id;
        }

        $.ajax({
            url : urlx
            , type :"POST"
            , data : JSON.stringify(pers.serialise())
            , contentType: "application/json; charset=utf-8"
            , dataType: "json",
        }).done(function(data){
            if (data.status == "00") {
                alert(data.message);
                index(); // Refresh Table
            }
            else {
                alert(data.message);
            }
        })
    })

    // Each input set to Object START

    $("#name").change(function(){
        pers.setName($(this).val());
    });

    $("#birthdate").change(function(){
        pers.setBirthDate($(this).val());
    });

    $("body").on("change" , ".parent-name" , function() {
        pers.addParentIndex($(this).data("parentrow") , $(this).val());

        console.log(pers.serialise())
    });

    // Each input set to Object END
});

function clearForm() {
    row = 1;
    $("#form-parent").html("");
    $("#form-parent").html(parent(row));
    
    $("#id").val(0);
    $("#name").val("");
    $("#birthdate").val("");
    
    row++;
}

function index() {
    $.ajax({
        url : "http://localhost:8080/dbox"
        , type :"GET"
    }).done(function(data){
        $("#tb_1-tbody").html("");
        
        $.each(data , function(key , val) {
            var tr = "<tr>"
                tr += "<td>"+val.id+"</td>"
                tr += "<td>"+val.name+"</td>"
                tr += "<td>"+val.age+"</td>"
                tr += "<td>"+val.birthdate+"</td>"
                tr += "<td><button class='btn btn-primary edit' data-row='"+val.id+"'>Edit</button></td>"
            tr += "</tr>"

            $("#tb_1-tbody").append(tr);

            // Create New Data START



            // Create New Data END
        })

        arr_pers = data;
        console.log(arr_pers);
        clearForm();
    })
}

function parent(row) {
    var html = "<tr id='parent-"+row+"'>";
    
        html += "<td><input name='par-name[]' id='par-name-"+row+"' class='parent-name' data-parentrow='"+row+"'></td>";

        if (row == 1) {
            html += "<td>&nbsp;</td>";
        }
        else {
            html += "<td><button class='del-parent' type='button' data-parentrow='"+row+"'>Hapus</button></td>";
        }

    html += "</tr>";

    return html;
}

// Object Person
function person() {
    this.id;
    this.name;
    this.birthdate;
    this.parent = new Array();
    this.parentIndex = new Array();

    this.setID = function(id) {
        this.id = id;
    }

    this.getID = function() {
        return this.id;
    }

    this.setName = function(name) {
        this.name = name;
    }

    this.getName = function() {
        return this.name;
    }

    this.setBirthDate = function(birthdate) {
        this.birthdate = birthdate;
    }

    this.getBirthDate = function() {
        return this.birthdate;
    }

    this.addParent = function(parent) {
        this.parent.push(parent);
    }

    this.setParent = function (index , parent) {
        this.parent[index] = parent;
    }

    this.addParentIndex = function(parentIndex , parent) {
        var index = this.parentIndex.indexOf(parentIndex);
        if (index == -1) {
            this.addParent(parent);

            this.parentIndex.push(parentIndex);
        }
        else {
            this.setParent(index , parent);
        }
    }

    this.deleteParent = function(parentIndex) {
        var index = this.parentIndex.indexOf(parentIndex);
        this.parentIndex.splice(index , 1);
        this.parent.splice(index , 1);
    }

    this.serialise = function() {
        return {
            id : this.id
            , name : this.name
            , birthdate : this.birthdate
            , parent : this.parent
        }
    }
}