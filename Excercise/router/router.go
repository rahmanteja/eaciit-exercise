package router

import (
	"hello/global"

	knot "github.com/eaciit/knot/knot.v1"

	"hello/Excercise/http/variable"

	"hello/Excercise/http/controller"
)

// Router a
type Router struct {
}

// Init a
// func (r Router) Init() {
// 	ks := new(knot.Server)

// 	ks.Address = variable.WebHost + ":" + variable.WebPort

// 	// Router Start
// 	ks.Register(new(controller.KnotbasicController), "")
// 	ks.Register(new(controller.DateTimeController), "")
// 	ks.Register(new(controller.PerlinNoiseController), "")
// 	ks.Register(new(controller.DBoxController), "")

// 	// Router End

// 	ks.Listen()
// }

// Debug a
func (r Router) Init() {

	app := knot.NewApp("Hello")
	app.ViewsPath = global.WebHTMLDir
	// app.Static("js", global.WebStatic+"js/")
	// app.Static("style", global.WebStatic+"style/")
	// app.Static("libjs", global.WebStatic+"lib/js/")
	// app.Static("libstyle", global.WebStatic+"lib/css/")
	// app.Static("upload", global.WebUploadDir)
	// app.Static("image", global.WebImageDir)

	app.Register(new(controller.KnotbasicController))
	app.Register(new(controller.DateTimeController))
	app.Register(new(controller.PerlinNoiseController))
	app.Register(new(controller.DBoxController))

	knot.RegisterApp(app)

	knot.StartApp(app, variable.WebHost+":"+variable.WebPort)
}
