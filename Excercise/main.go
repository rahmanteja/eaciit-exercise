package main

import (
	"hello/Excercise/http/variable"
	"hello/Excercise/router"
)

func main() {

	variable.DBConn = router.DBConn{}.Init()
	router.Router{}.Init()
	// router.Router{}.Init()
}
