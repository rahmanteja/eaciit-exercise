package model

// KnotBasic Return to View
type KnotBasic struct {
	STATUS string `json:"status,omitempty"`
	DATE   string `json:"date,omitempty"`
}
