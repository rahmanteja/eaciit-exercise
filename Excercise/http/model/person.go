package model

import "gopkg.in/mgo.v2/bson"

// Person a
var Person struct {
	ID        bson.ObjectId
	Nama      string
	Birthdate string
	Age       int
	Parent    []string
	// NAMA string `json:"name,omitempty"`
	// UMUR int    `json:"umur,omitempty"`
}
