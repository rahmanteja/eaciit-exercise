package variable

import (
	"os"
)

// Setting For Web
const (
	WebHost string = "localhost"
	WebPort string = "8081"
)

// Dir a
var Dir = func() string {
	var dir, err = os.Getwd()
	if err != nil {
		panic(err)
	}

	return dir
}()

// Resource aas
var (
	Resource string = Dir + "/resource/"
	Static   string = Resource + "/public/"
	View     string = Resource + "/view/"
)
