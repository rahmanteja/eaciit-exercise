package controller

import (
	"hello/Excercise/http/model"
	"time"

	knot "github.com/eaciit/knot/knot.v1"
)

// KnotbasicController a
type KnotbasicController struct {
}

// Index a
func (knotCont KnotbasicController) Index(r *knot.WebContext) interface{} {
	r.Config.OutputType = knot.OutputJson

	var knots model.KnotBasic
	knots.STATUS = "00"
	knots.DATE = time.Now().Format("2006-01-02T15:04:05-0700")

	return knots
}
