package controller

import (
	"math"
	"math/rand"
	"sync"

	"github.com/eaciit/knot/knot.v1"
)

// PerlinNoiseController a
type PerlinNoiseController struct {
}

var mx sync.RWMutex
var wg sync.WaitGroup
var arNoise [][]float32
var arMax []float32
var w, h int

// Index a
func (p PerlinNoiseController) Index(r *knot.WebContext) interface{} {
	w = 10 // INPUT
	h = 10 // INPUT

	arMax = arMax[:0]

	GridDefinition()

	wg.Add(w * h)

	for i := 0; i < w; i++ {
		for j := 0; j < h; j++ {
			// go powMe(&wg, row, &sts)
			go DotProduct(i, j)
		}
	}

	wg.Wait()

	r.Config.OutputType = knot.OutputJson

	return arMax
}

// GridDefinition a
func GridDefinition() {
	// var noise = make([][]float32, x)
	arNoise = make([][]float32, w)

	for i := 0; i < w; i++ {
		var noiseY = make([]float32, h)
		arNoise[i] = noiseY

		for j := 0; j < h; j++ {
			arNoise[i][j] = rand.Float32()
		}
	}

}

// DotProduct a
func DotProduct(i, j int) {
	mx.Lock()

	// set cons
	var samplePeriod float64 = math.Pow(2, 1)
	var freq float32 = float32(1) / float32(samplePeriod)

	//calculate the horizontal sampling indices
	smple_i0 := (i / int(samplePeriod)) * int(samplePeriod)
	smple_i1 := (smple_i0 + int(samplePeriod)) % w
	h_blend := (float32(i) - float32(smple_i0)) * freq

	// calculate the vertical sampling indices
	smple_j0 := (j / int(samplePeriod)) * int(samplePeriod)
	smple_j1 := (smple_j0 + int(samplePeriod)) % h
	v_blend := (float32(j) - float32(smple_j0)) * freq

	//blend the top two corners
	top := Interpolate(arNoise[smple_i0][smple_j0], arNoise[smple_i1][smple_j0], h_blend)

	//blend the bottom two corners
	bot := Interpolate(arNoise[smple_i0][smple_j1], arNoise[smple_i1][smple_j1], h_blend)

	// return Interpolate(top, bot, v_blend)
	arMax = append(arMax, Interpolate(top, bot, v_blend))
	// arMax[row] = Interpolate(top, bot, v_blend)

	// row++
	defer mx.Unlock()
	defer wg.Done()

}

// Interpolate a
func Interpolate(x0, x1, alpha float32) float32 {
	return x0*(1-alpha) + alpha*x1
}
