package controller

import (
	"hello/Excercise/http/model"
	"time"

	"github.com/eaciit/knot/knot.v1"
)

// DateTimeController a
type DateTimeController struct {
}

// Index a
func (d DateTimeController) Index(r *knot.WebContext) interface{} {

	r.Config.OutputType = knot.OutputJson

	var knots model.KnotBasic
	knots.STATUS = "00"
	knots.DATE = time.Now().Add(time.Hour * 12).Format("2006-01-02T15:04:05-0700")

	return knots
}
