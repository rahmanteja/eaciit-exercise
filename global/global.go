package global

// Setting For Web
const (
	WebHost      string = ""
	WebPort      string = "8080"
	WebGoPath    string = "C:/Users/SW/go/src/" // GOPATH DIPC SAYA , SILAHKAN UBAH
	WebDir       string = WebGoPath + "hello/"
	WebUploadDir string = WebDir + "resource/public/file/upload/"
	WebImageDir  string = WebDir + "resource/public/file/static/image/"
	WebHTMLDir   string = WebDir + "resource/view/"
	WebStatic    string = WebDir + "resource/public/"
)

// User Default
const (
	UserSystem string = "0"
)

// DB Credential
const (
	DBHost string = "localhost"
)

// Contain Message
const (
	MsgSuccess        string = "00"
	MsgFailedUnkown   string = "-01"
	MsgFailedValidate string = "-02"
)
