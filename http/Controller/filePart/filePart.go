package filePart

import (
	"fmt"
	"hello/global"
	"hello/http/Model"
	"hello/router/templating"
	"io"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

var arFile []Model.File

// Index a
func Index(w http.ResponseWriter, r *http.Request) {
}

// Create a
func Create(w http.ResponseWriter, r *http.Request) {
	templating.ToView(w, "Upload/upload.html", nil)
}

// Store a
func Store(w http.ResponseWriter, r *http.Request) {

	// Cek Method used

	file, header, err := r.FormFile("fileupload")

	if err != nil {
		fmt.Fprintf(w, "File not avaiable")
		return
	}
	defer file.Close()

	fName := header.Filename

	// Create Folder
	// Users\SW\go\src\hello\fileUpload\upl
	out, err := os.Create(global.WebUploadDir + fName)
	if err != nil {
		fmt.Fprintf(w, "Unable to create the file for writing. Check your write access privilege")
		fmt.Fprintln(w, err)
		return
	}
	defer out.Close()

	// Using POST request upload file
	_, err = io.Copy(out, file)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	// var files Model.File
	// files.ID = generateID()
	// files.FILENAME = fName
	// files.LINK = "http://localhost:8080/upload/file/" + fName
	// files.CD = time.Now().Format("2006-01-02T15:04:05-0700")
	// files.CB = global.UserSystem

	fmt.Fprintf(w, "File uploaded successfully : ")
	fmt.Fprintf(w, "http://localhost:8080/upload/file/"+fName)
	fmt.Fprintf(w, "\n Copy Link diatas maka akan mendownload file yg sudah diupload")

	// templating.ToView(w, "Upload/uploadLink.html", nil)
}

// Edit a
func Edit(w http.ResponseWriter, r *http.Request) {
}

// Update a
func Update(w http.ResponseWriter, r *http.Request) {
}

// Destroy a
func Destroy(w http.ResponseWriter, r *http.Request) {
}

// Search a
func Search(w http.ResponseWriter, r *http.Request) {

	// untuk sementara anggep download aja dulu
	params := mux.Vars(r)

	// w.Header().Set("Content-Type", "applicaiton/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename='"+params["fname"]+"'")
	http.ServeFile(w, r, global.WebUploadDir+params["fname"])

}

func generateID() int {
	var id int

	for _, el := range arFile {
		if id < el.ID {
			id = el.ID
		}
	}

	return id + 1
}
