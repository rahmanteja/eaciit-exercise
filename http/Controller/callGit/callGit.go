package CallGit

import (
	"encoding/json"
	"fmt"
	"hello/http/Model"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

// Index a
func Index(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	resp, err := http.Get("https://api.github.com/users/" + params["id"])
	// resp , err := http.Get("http://localhost:8080/knotbasic")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var user Model.User
	err = json.Unmarshal([]byte(string(body)), &user)
	// err = json.NewDecoder(string(body)).Decode(&user)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(body) + " >> " + user.LOGIN)

	json.NewEncoder(w).Encode(user)
}

func keepline(s string, i int) string {
	res := strings.Join(strings.Split(s, "\n")[:i], "\n")

	return strings.Replace(res, "\r", "", -1)
}
