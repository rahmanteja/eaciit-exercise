package knotbasic

import (
	"encoding/json"
	"hello/global"
	"hello/http/Model"
	"net/http"
	"time"
)

// Index a
func Index(w http.ResponseWriter, r *http.Request) {
	var msg Model.DateTime

	msg.STATUS = global.MsgSuccess
	msg.DATE = time.Now().Format("2006-01-02T15:04:05-0700")

	json.NewEncoder(w).Encode(msg)
}
