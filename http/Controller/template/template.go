package main

import (
	"hello/global"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
)

// Todo a
type Todo struct {
	Title string
	Done  bool
}

// TodoPageData a
type TodoPageData struct {
	PageTitle string
	Todos     []Todo
}

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/template", Index).Methods(http.MethodGet)

	http.ListenAndServe(":8080", router)
}

// Index a
func Index(w http.ResponseWriter, r *http.Request) {
	strv := global.WebHTMLDir
	tmpl := template.Must(template.ParseFiles(strv + "Template/layout.html"))

	data := TodoPageData{
		PageTitle: "My TODO list xxxx",
		Todos: []Todo{
			{Title: "<i>Task 1</i>", Done: false},
			{Title: "Task 2", Done: true},
			{Title: "Task 3", Done: true},
		},
	}
	tmpl.Execute(w, data)
}
