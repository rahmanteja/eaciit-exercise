package DBoxModel

import (
	"encoding/json"
	"hello/global"
	"hello/http/Model"
	"hello/router/templating"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

// Message a
type Message struct {
	STATUS  string `json:"status,omitempty"`
	MESSAGE string `json:"message,omitempty"`
}

// People a
var People []Model.Person

// Index Yg akan dibuka pertama
func Index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(People)
}

// Create a
func Create(w http.ResponseWriter, r *http.Request) {
	templating.ToView(w, "Person/person.html", nil)
	// fmt.Println(http.Dir(global.WebHTMLDir + "Person/person.html"))
	// http.FileServer(http.Dir(global.WebHTMLDir + "Person/person.html"))
}

// Store a
func Store(w http.ResponseWriter, r *http.Request) {
	// params := mux.Vars(r)
	var person Model.Person
	var msg Message

	err := json.NewDecoder(r.Body).Decode(&person)
	if err != nil {
		json.NewEncoder(w).Encode("Failed Insert")
		panic(err)
	}

	// Validate body START

	if person.NAME == "" {
		msg.STATUS = global.MsgFailedValidate
		msg.MESSAGE = "Name is Required"

		json.NewEncoder(w).Encode(msg)
		return
	}

	if person.BIRTHDATE == "" {
		msg.STATUS = global.MsgFailedValidate
		msg.MESSAGE = "Birthdate is Required"

		json.NewEncoder(w).Encode(msg)
		return
	}

	// Validate body END

	// person.NAME = params["name"]
	person.ID = generateID()
	person.AGE = generateAge(person.BIRTHDATE)
	person.CREATED_DATE = time.Now().Format("2006-01-02T15:04:05-0700")
	person.CREATED_BY = global.UserSystem

	People = append(People, person)

	msg.STATUS = global.MsgSuccess
	msg.MESSAGE = "Success Insert"

	json.NewEncoder(w).Encode(msg)
}

// Edit a
func Edit(w http.ResponseWriter, r *http.Request) {
}

// Update a
func Update(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var msg Message
	var person Model.Person
	var indx int

	if len(People) == 0 {
		msg.STATUS = global.MsgFailedValidate
		msg.MESSAGE = "No Data Found for " + params["id"]

		json.NewEncoder(w).Encode(msg)
		return
	}

	for index, el := range People {
		id, err := strconv.ParseInt(params["id"], 0, 64)
		if err != nil {
			panic(err)
		}

		if id == el.ID {
			person = el
			indx = index
			break
		}
	}

	if person.ID == 0 {
		msg.STATUS = global.MsgFailedValidate
		msg.MESSAGE = "No Data Found for " + params["id"]

		json.NewEncoder(w).Encode(msg)
		return
	}

	var dat map[string]interface{}

	err := json.NewDecoder(r.Body).Decode(&dat)
	if err != nil {
		msg.STATUS = global.MsgFailedUnkown
		msg.MESSAGE = "Failed Unkown When Update"

		json.NewEncoder(w).Encode(msg)
		panic(err)
	}

	// Validate body START

	if dat["name"] == nil {
		msg.STATUS = global.MsgFailedValidate
		msg.MESSAGE = "Name is Required"

		json.NewEncoder(w).Encode(msg)
		return
	}

	if dat["birthdate"] == nil {
		msg.STATUS = global.MsgFailedValidate
		msg.MESSAGE = "Birthdate is Required"

		json.NewEncoder(w).Encode(msg)
		return
	}

	// Validate body END
	person.NAME = dat["name"].(string)
	person.BIRTHDATE = dat["birthdate"].(string)

	// Parsing Parent
	arIParent := dat["parent"].([]interface{})
	arSParent := make([]string, len(arIParent))
	for row, v := range arIParent {
		arSParent[row] = v.(string)
	}

	person.PARENT = arSParent
	person.AGE = generateAge(person.BIRTHDATE)
	person.UPDATED_DATE = time.Now().Format("2006-01-02T15:04:05-0700")
	person.UPDATED_BY = global.UserSystem

	People[indx] = person

	msg.STATUS = global.MsgSuccess
	msg.MESSAGE = "Success Update"

	json.NewEncoder(w).Encode(msg)
}

// Destroy a
func Destroy(w http.ResponseWriter, r *http.Request) {
}

// Search a
func Search(w http.ResponseWriter, r *http.Request) {
	//http.Redirect(w, r, "http://localhost:8080/views/Person/person.html", 200)

	templating.ToView(w, "Person/person.html", nil)
}

func generateID() int64 {
	var id int64

	for _, el := range People {
		if id < el.ID {
			id = el.ID
		}
	}

	return id + 1
}

func generateAge(sBDate string) int {
	bDate, err := time.Parse("2006-01-02", sBDate)

	if err != nil {
		panic(err)
	}

	return int(time.Since(bDate) / time.Hour / 24 / 365)
}
