package Model

// File a
type File struct {
	ID       int    `json:"id,omitempty"`
	FILENAME string `json:"filename,omitempty"`
	LINK     string `json:"link,omitempty"`
	CD       string `json:"cd,omitempty"`
	CB       string `json:"cb,omitempty"`
	UD       string `json:"ud,omitempty"`
	UB       string `json:"ub,omitempty"`
}
