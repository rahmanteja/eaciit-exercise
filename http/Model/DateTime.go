package Model

// DateTime Is return value to client
type DateTime struct {
	STATUS string `json:"status,omitempty"`
	DATE   string `json:"date,omitempty"`
}
