package Model

// Person a
type Person struct {
	ID           int64    `json:"id,omitempty"`
	NAME         string   `json:"name,omitempty"`
	AGE          int      `json:"age,omitempty"`
	BIRTHDATE    string   `json:"birthdate,omitempty"`
	PARENT       []string `json:"parent,omitempty"`
	CREATED_DATE string   `json:"created_date , omitempty"`
	CREATED_BY   string   `json:"created_by , omitempty"`
	UPDATED_DATE string   `json:"updated_date , omitempty"`
	UPDATED_BY   string   `json:"updated_by , omitempty"`
}
