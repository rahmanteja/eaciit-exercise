package Model

// User s
type User struct {

	// DATE string `json:"date,omitempty"`

	LOGIN     string `json:"login,omitempty"`
	NAME      string `json:"name,omitempty"`
	COMPANY   string `json:"company,omitempty"`
	CREATEDAT string `json:"created_at,omitempty"` //timestamps return in ISO 8601 format
	UPDATEDAT string `json:"updated_at,omitempty"` //timestamps return in ISO 8601 format
}
