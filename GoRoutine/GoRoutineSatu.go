package main

import (
	"fmt"
	"math"
	"math/rand"
	"sync"
)

// Perlin a
type Perlin struct {
	status  int
	arNoise [][]float32
	arMax   []float32
	w, h    int
}

var mx sync.RWMutex
var wg sync.WaitGroup

func main() {
	var p = new(Perlin)
	p.Init()
}

// Init a
func (p *Perlin) Init() {
	p.status = 1

	var w int = 10 // Input
	var h int = 10 // Input

	// Coordinates := Coordinate{w, h}
	p.w = w
	p.h = h

	// arMax = make([]int, max)
	x := p.w
	y := p.h

	GridDefinition(p)

	wg.Add(x * y)

	for i := 0; i < x; i++ {
		for j := 0; j < y; j++ {
			// go powMe(&wg, row, &sts)
			go DotProduct(i, j, p)
		}
	}

	wg.Wait()

	fmt.Println("NOISE DEFINITION")
	fmt.Printf("%v \n", p.arNoise)
	fmt.Printf("%v \n", len(p.arNoise))
	fmt.Println("HASIL")
	fmt.Printf("%v \n", p.arMax)
	fmt.Printf("%v \n", len(p.arMax))
	fmt.Println("DONE")
}

// GridDefinition a
func GridDefinition(p *Perlin) {
	x := p.w
	y := p.h

	// var noise = make([][]float32, x)

	for i := 0; i < x; i++ {
		var noiseY = make([]float32, y)
		p.arNoise = append(p.arNoise, noiseY)
		// noise[i] = noiseY

		for j := 0; j < y; j++ {
			p.arNoise[i][j] = rand.Float32()
		}
	}

}

// DotProduct a
func DotProduct(i, j int, sts *Perlin) {
	mx.Lock()

	x := sts.w
	y := sts.h

	// set cons
	var samplePeriod float64 = math.Pow(2, 1)
	var freq float32 = float32(1) / float32(samplePeriod)

	//calculate the horizontal sampling indices
	smple_i0 := (i / int(samplePeriod)) * int(samplePeriod)
	smple_i1 := (smple_i0 + int(samplePeriod)) % x
	h_blend := (float32(i) - float32(smple_i0)) * freq

	// calculate the vertical sampling indices
	smple_j0 := (j / int(samplePeriod)) * int(samplePeriod)
	smple_j1 := (smple_j0 + int(samplePeriod)) % y
	v_blend := (float32(j) - float32(smple_j0)) * freq

	//blend the top two corners
	top := Interpolate(sts.arNoise[smple_i0][smple_j0], sts.arNoise[smple_i1][smple_j0], h_blend)

	//blend the bottom two corners
	bot := Interpolate(sts.arNoise[smple_i0][smple_j1], sts.arNoise[smple_i1][smple_j1], h_blend)

	// return Interpolate(top, bot, v_blend)
	sts.arMax = append(sts.arMax, Interpolate(top, bot, v_blend))

	defer mx.Unlock()
	defer wg.Done()

}

// Interpolate a
func Interpolate(x0, x1, alpha float32) float32 {
	return x0*(1-alpha) + alpha*x1
}
